Rails.application.routes.draw do
  scope "(:locale)", :locale => /en|ru/ do
    root :to => 'home#index'

    get 'signin' => 'user_sessions#new', :as => :signin
    post 'signin' => 'user_sessions#create'
    match 'signout' => 'user_sessions#destroy', :as => :signout, via: [:get, :post]

    match 'login' => 'distillator#login', :as => :login, via: [:get, :post]
    match 'get_all_user' => 'distillator#get_all_user', :as => :get_all_user, via: [:get, :post]
    match 'set_parameters' => 'distillator#set_parameters', :as => :set_parameters, via: [:get, :post]
    match 'get_parameters' => 'distillator#get_parameters', :as => :get_parameters, via: [:get, :post]
    match 'get_user_in_radius' => 'distillator#get_user_in_radius', :as => :get_user_in_radius, via: [:get, :post]


    get 'signup' => 'users#new', :as => :signup
    post 'signup' => 'users#create'
  end
  get "/" => "home#index"
end
