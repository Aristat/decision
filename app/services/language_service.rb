class LanguageService
  class << self
    def enabled_conditions
      ["languages.enabled = ?", true]
    end
  end
end
