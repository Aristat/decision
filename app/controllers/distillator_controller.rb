class DistillatorController < ApplicationController

  def login
    uniq_key = SecureRandom.urlsafe_base64
    render :json => {uniq_key: uniq_key}.to_json
  end

  def get_all_user
    render :json => {users: Player.all.map(&:attributes)}.to_json
  end

  # http://localhost:3000/set_parameters?uniq_key=1&coordinate_x=12&coordinate_y=15&angle=5
  def set_parameters
    check_params(params)
    raise "Need angle" unless params["angle"]
    player = Player.find_by_uniq_key(params["uniq_key"])
    if player.nil?
      player = Player.create!(uniq_key: params["uniq_key"], coordinate_x: params["coordinate_x"], coordinate_y: params["coordinate_y"], angle: params["angle"])
    else
      player.update(coordinate_x: params["coordinate_x"], coordinate_y: params["coordinate_y"], angle: params["angle"])
    end
    render :json => {success: player.attributes}.to_json
  end

  # http://localhost:3000/get_user_in_radius?uniq_key=1&radius=5
  def get_user_in_radius
    raise "Params is empty" unless params
    raise "Need uniq key" unless params["uniq_key"]
    raise "Need radius" unless params["radius"]
    array_out = []
    current_player = Player.find_by_uniq_key(params["uniq_key"])
    raise "User is empty" if current_player.nil?
    sqr_radius = params["radius"].to_f * params["radius"].to_f
    Player.all.map do |player|
      next if player == current_player
      dx = current_player.coordinate_x - player.coordinate_x
      dy = current_player.coordinate_y - player.coordinate_y
      unless sqr_radius <= (dx*dx + dy*dy)
        array_out << player.attributes
      end
    end
    render :json => array_out.to_json
  end

  def get_parameters
    raise "Params is empty" unless params
    raise "Need uniq key" unless params["uniq_key"]
    render :json => {success: Player.find_by_uniq_key(params["uniq_key"])}.to_json
  end

  def check_params(params)
    raise "Params is empty" unless params
    raise "Need uniq key" unless params["uniq_key"]
    raise "Need coordinate_x" unless params["coordinate_x"]
    raise "Need coordinate_y" unless params["coordinate_y"]
  end
end