class UserSessionsController < ApplicationController

  layout 'greetings'

  before_filter :require_no_user, only: [:new, :create]
  before_filter :require_user, only: :destroy

  def new
    @user_session = UserSession.new
  end

  def create
    @user_session = UserSession.new(params[:user_session])
    # todo: Добавить подтверждение регистрации по мейлу
    # user = User.find_by_login(@user_session.login)
    # p @user_session.valid?
    # p user.present?

    if @user_session.save
      I18n.locale = current_user.language.code
      flash[:notice] = I18n.t("activeview.flashes.user_session.notices.created")
      redirect_to root_url
    else
      render action: :new
    end
  end

  def destroy
    current_user_session.destroy
    flash[:notice] = I18n.t("activeview.flashes.user_session.notices.deleted")
    redirect_to root_url
  end

  private

  def user_session_params
    params.require(:user_session).permit(:login, :password, :remember_me)
  end
end
