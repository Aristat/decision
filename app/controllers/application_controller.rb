class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper :all
  helper_method :current_user_session, :current_user, :current_language

  before_filter :set_locale
  before_filter :load_language
  before_filter :validate_language

  private

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def validate_language
    return nil unless current_user.present?
    language_code = current_user.language.code.to_sym
    p I18n.locale
    if I18n.locale != language_code
      I18n.locale = language_code
      redirect_to root_url(locale: I18n.locale)
    end
  end

  def load_language
    current_language
  end

  def current_language
    @current_language = Language.find_by_code(I18n.locale.to_s)
  end

  def current_user_session
    @current_user_session ||= UserSession.find
  end

  def current_user
    @current_user ||= current_user_session && current_user_session.user
  end

  def require_user
    unless current_user
      store_location
      flash[:notice] = I18n.t('navigation.application.mainmenu.service')
      redirect_to signin_url
      return false
    end
  end

  def require_no_user
    if current_user
      flash[:notice] = I18n.t('navigation.application.mainmenu.service')
      redirect_to root_url
      return false
    end
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge options
  end
end
