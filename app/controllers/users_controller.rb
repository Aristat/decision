class UsersController < ApplicationController

  layout 'greetings'

  def new
    load_data
    @user = User.new
  end

  def create
    @user = User.new(options)

    if @user.save
      redirect_to root_url
    else
      load_data
      render :action => :new
    end
  end

  private

  def users_params
    params.require(:user).permit(:language_id, :currency_id, :login, :email, :password, :password_confirmation)
  end

  def options
    @options = users_params.merge({language_id: @current_language.id })
  end

  def load_data
    @languages = Language.includes(:translations).where(enabled: true).order('language_translations.name').all
  end
end
