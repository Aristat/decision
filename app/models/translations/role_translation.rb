class RoleTranslation < ActiveRecord::Base
  belongs_to :role

  validates_presence_of :locale
  validates_presence_of :name
  validates_presence_of :about

  validates_length_of :locale, :within => 1..16
  validates_length_of :name, :within => 1..128
  validates_length_of :about, :within => 1..128

  validates_uniqueness_of :name, :scope => [:locale]
  validates_uniqueness_of :locale, :scope => [:name]
end
