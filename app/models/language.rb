class Language < ActiveRecord::Base
  has_many :users, :dependent => :destroy
  has_many :currencies, :dependent => :destroy
  has_many :language_translations, :dependent => :destroy

  translates :name, :about

  validates_presence_of :code

  validates_length_of :code, :within => 1..128

  validates_uniqueness_of :code

  attr_accessible :code, :enabled
  attr_accessible :language_translations_attributes, :locale, :name, :about
  accepts_nested_attributes_for :language_translations, :allow_destroy => true

  def self.default_language
    self.where(code: "en").first
  end
end
