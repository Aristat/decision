class User < ActiveRecord::Base
  acts_as_authentic do |configuration|
    configuration.crypto_provider = Authlogic::CryptoProviders::Sha512
  end

  belongs_to :language
  belongs_to :currency

  has_many :user_roles, :dependent => :destroy
  has_many :roles, through: :user_roles

  validate :valid_currency_by_language

  attr_accessible :language_id, :currency_id, :login, :email, :password, :password_confirmation

  def user?
    has_role?("user")
  end

  def admin?
    has_role?("admin")
  end

  private

  def has_role?(role, subject = nil)
    user_roles.each do |user_role|
      return true if user_role.role.code.to_s == role.to_s
    end
    false
  end

  def valid_currency_by_language
    errors.add(:currency_id, I18n.t("activerecord.errors.models.user.currency")) unless self.language.present? &&
        self.language.currencies.find_by_id(self.currency_id).present?
  end
end
