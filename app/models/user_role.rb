class UserRole < ActiveRecord::Base
  # ::Rails.logger.error("...")
  
  belongs_to :user
  belongs_to :role

  validates_presence_of :user_id
  validates_presence_of :role_id

  validates_uniqueness_of :user_id, :scope => :role_id
  validates_uniqueness_of :role_id, :scope => :user_id

  attr_accessible :user_id, :role_id
end
