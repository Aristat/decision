class Role < ActiveRecord::Base
  # ::Rails.logger.error("...")
  has_many :user_roles, :dependent => :destroy
  has_many :role_translations, :dependent => :destroy

  translates :name, :about

  validates_presence_of :code

  validates_length_of :code, :within => 1..128

  validates_uniqueness_of :code

  attr_accessible :code
  attr_accessible :role_translations_attributes, :locale, :name, :about
  accepts_nested_attributes_for :role_translations, :allow_destroy => true
end
