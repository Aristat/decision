class Currency < ActiveRecord::Base
  # ::Rails.logger.error("...")

  belongs_to :language
  has_many :currency_translations, :dependent => :destroy

  translates :name, :about

  validates_presence_of :code

  validates_length_of :code, :within => 1..128

  validates_uniqueness_of :code, scope: [:language_id]

  attr_accessible :language_id, :code, :enabled, :prime
  attr_accessible :currency_translations_attributes, :locale, :name, :about
  accepts_nested_attributes_for :currency_translations, :allow_destroy => true

  # Return code
  def self.get_default_for(locale)
    language = Language.find_by_code(locale)
    Currency.where(language_id: language.id, prime: true).first
  end

  def self.get_valid_currency(locale, currency)
    language = Language.find_by_code(locale)
    return self.get_default_for(I18n.locale) unless language.present?
    currency = language.currencies.find_by_code(currency)
    return currency.present? ? currency : self.get_default_for(locale)
  end
end
