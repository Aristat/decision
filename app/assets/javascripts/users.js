var show_language_currencies;

show_language_currencies = function(all_currencies, language) {
    var currencies;
    currencies = $(all_currencies).filter("optgroup[label='" + language + "']").html();
    if (currencies) {
        return $("select#user_currency_id").html(currencies);
    } else {
        return $("select#user_currency_id").html(all_currencies);
    }
};

$.currencies_load = function(){
    var all_currencies, language;
    all_currencies = $("select#user_currency_id").html();
    language = $("select#user_currency_id").data("current-language");
    show_language_currencies(all_currencies, language);
    return $("select#user_language_id").change(function() {
        language = $("select#user_language_id :selected").text();
        return show_language_currencies(all_currencies, language);
    });
};

$(document).ready($.currencies_load);
$(document).on('page:load',$.currencies_load);

