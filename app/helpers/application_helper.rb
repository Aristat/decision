module ApplicationHelper

  def has_flash_any_key_with_message?
    return true if flash.key?(:notice) && flash[:notice].present?
    return true if flash.key?(:information) && flash[:information].present?
    return true if flash.key?(:warning) && flash[:warning].present?
    return true if flash.key?(:error) && flash[:error].present?
    return false
  end

  def store_location
    session[:return_to] = env["REQUEST_PATH"]
    session[:return_to_query] = env["QUERY_STRING"]
  end

  def locale_or_nil(code=I18n.locale)
    code = I18n.locale if code.nil?
    (code.to_sym == :en) ? nil : code
  end

  def link_to_social_network(name, url)
    link_to "", url, { :id => name, class: "new-window" }
  end
end
