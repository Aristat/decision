module LanguagesHelper
  def make_url_for_en
    locale = :en
    options = prepare_params
    options.merge!( locale: locale, only_path: false )
    options.merge!( exclusions_options )
    url_for(options)
  end

  def make_url_for_ru
    locale = :ru
    options = prepare_params
    options.merge!( locale: locale.to_s, currency: nil, only_path: false )
    options.merge!( exclusions_options )
    url_for(options)
  end

  private

  def prepare_params
    opts = params.dup
    opts.reject! { |key, value| value.is_a?(Hash) ||
        ["utf8", "authenticity_token", "commit"].include?(key) ||
        value.blank? }
    opts
  end

  def exclusions_options
    options = {}
    if controller_name == "sessions" && action_name == "create"
      options.merge!(use_route: "signin", action: nil)
    end
    options
  end
end
