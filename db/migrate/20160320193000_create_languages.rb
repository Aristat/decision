class CreateLanguages < ActiveRecord::Migration
  def self.up
    create_table :languages do |t|
      t.column :code, :string, :limit => 128, :null => false
      t.column :enabled, :boolean, :null => false, :default => false
      t.timestamps
    end

    # Add foreign keys

    # Create indexes
    add_index :languages, :code, :unique => true

    # Localization table
    Language.create_translation_table! :name => {:type => :string, :limit => 128, :null => false, :default => "N/A"},
                                       :about => {:type => :string, :limit => 128, :null => false, :default => "N/A"}
    add_index :language_translations, [:name, :locale], :unique => true

  end

  def self.down
    Language.drop_translation_table!
    drop_table :languages
  end
end
