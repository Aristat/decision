class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :uniq_key
      t.float :coordinate_x
      t.float :coordinate_y
      t.float :angle

      t.timestamps null: false
    end
  end
end
