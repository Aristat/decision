class CreateCurrencies < ActiveRecord::Migration
  def self.up
    create_table :currencies do |t|
      t.column :language_id, :integer, :null => false
      t.column :code, :string, :limit => 128, :null => false
      t.column :prime, :boolean, :null=>false, :default => false
      t.column :enabled, :boolean, :null=> false, :default => false
      t.timestamps
    end

    # Add foreign keys
    add_foreign_key :currencies, :languages, :column => "language_id"

    # Create indexes
    add_index :currencies, :language_id
    add_index :currencies, :code, :unique => true

    # Localization table
    Currency.create_translation_table! :name => {:type => :string, :limit => 128, :null => false, :default => "N/A"},
                                       :about => {:type => :string, :limit => 128, :null => false, :default => "N/A"}
    add_index :currency_translations, [:name, :locale], :unique => true
  end

  def self.down
    Currency.drop_translation_table!
    drop_table :currencies
  end
end
