class CreateRoles < ActiveRecord::Migration
  def self.up
    create_table :roles do |t|
      t.column :code, :string, :limit => 128, :null => false
      t.timestamps
    end

    # Add foreign keys

    # Create indexes
    add_index :roles, :code, :unique => true

    # Localization table
    Role.create_translation_table! :name => {:type => :string, :limit => 128, :null => false, :default => "N/A"},
                                   :about => {:type => :string, :limit => 128, :null => false, :default => "N/A"}
    add_index :role_translations, [:name, :locale], :unique => true
  end

  def self.down
    Role.drop_translation_table!
    drop_table :roles
  end
end
