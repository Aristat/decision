class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :language_id, :null => false
      t.integer :currency_id, :null => false
      t.string :login, :null => false
      t.string :email, :null => false
      t.string :crypted_password, :null => false
      t.string :password_salt, :null => false
      t.string :persistence_token, :null => false

      t.timestamps null: false
    end

    add_index :users, :email, unique: true
  end
end
