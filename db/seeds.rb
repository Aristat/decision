%w{
  main
  users
}.each do |part|
  require File.expand_path(File.dirname(__FILE__))+"/seeds/#{part}_seed.rb"
end