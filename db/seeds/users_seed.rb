sysadmin_role = Role.find_by_code("sysadmin")
if sysadmin_role.nil?
  I18n.locale = :en
  sysadmin_role = Role.create! :name => "Sysadmin", :code => "sysadmin", :about => "Sysadmin"
  I18n.locale = :ru
  sysadmin_role.update_attributes!(:name => "Системный администратор", :about => "Системный администратор")
end

monitor_role = Role.find_by_code("monitor")
if monitor_role.nil?
  I18n.locale = :en
  monitor_role = Role.create! :name => "Monitor", :code => "monitor", :about => "Monitor"
  I18n.locale = :ru
  monitor_role.update_attributes!(:name => "Наблюдающий", :about => "Системный Наблюдающий")
end

moderator_role = Role.find_by_code("moderator")
if moderator_role.nil?
  I18n.locale = :en
  moderator_role = Role.create! :name => "Moderator", :code => "moderator", :about => "Moderator"
  I18n.locale = :ru
  moderator_role.update_attributes!(:name => "Модератор", :about => "Модератор")
end

hrmanager_role = Role.find_by_code("hrmanager")
if hrmanager_role.nil?
  I18n.locale = :en
  hrmanager_role = Role.create! :name => "HR manager", :code => "hrmanager", :about => "HR manager"
  I18n.locale = :ru
  hrmanager_role.update_attributes!(:name => "Служба персонала", :about => "Служба персонала")
end

supporter_role = Role.find_by_code("supporter")
if supporter_role.nil?
  I18n.locale = :en
  supporter_role = Role.create! :name => "Supporter", :code => "supporter", :about => "Supporter"
  I18n.locale = :ru
  supporter_role.update_attributes!(:name => "Поддержка", :about => "Поддержка")
end

salesman_role = Role.find_by_code("salesman")
if salesman_role.nil?
  I18n.locale = :en
  salesman_role = Role.create! :name => "Salesman", :code => "salesman", :about => "Salesman"
  I18n.locale = :ru
  salesman_role.update_attributes!(:name => "Продавец", :about => "Продавец")
end

employer_role = Role.find_by_code("employer")
if employer_role.nil?
  I18n.locale = :en
  employer_role = Role.create! :name => "Employer", :code => "employer", :about => "Employer"
  I18n.locale = :ru
  employer_role.update_attributes!(:name => "Работодатель", :about => "Работодатель")
end

employee_role = Role.find_by_code("employee")
if employee_role.nil?
  I18n.locale = :en
  employee_role = Role.create! :name => "Employee", :code => "employee", :about => "Employee"
  I18n.locale = :ru
  employee_role.update_attributes!(:name => "Сотрудник", :about => "Сотрудник")
end