english_language = Language.find_by_code("en")
if english_language.nil?
  I18n.locale = :en
  english_language = Language.create!(:name => "English", :code => "en", :about => "English", :enabled => true)
  I18n.locale = :ru
  english_language.update_attributes!(:name => "Английский", :about => "Английский")
end

russian_language = Language.find_by_code("ru")
if russian_language.nil?
  I18n.locale = :en
  russian_language = Language.create!(:name => "Russian", :code => "ru", :about => "Russian", :enabled => true)
  I18n.locale = :ru
  russian_language.update_attributes!(:name => "Русский", :about => "Русский")
end

en_usd_currency = Currency.find_by_language_id_and_code(english_language.id, "usd")
if en_usd_currency.nil?
  I18n.locale = :en
  en_usd_currency = Currency.create!(:language_id => english_language.id, :name => "USD", :code => "usd",
                                      :about => "USA dollar", :enabled => true, :prime => true)
  I18n.locale = :ru
  en_usd_currency.update_attributes!(:name => "Доллар США", :about => "Доллар США")
end

en_eur_currency = Currency.find_by_language_id_and_code(english_language.id, "eur")
if en_eur_currency.nil?
  I18n.locale = :en
  en_eur_currency = Currency.create!(:language_id => english_language.id, :name => "EUR", :code => "eur",
                                      :about => "EUR", :enabled => true)
  I18n.locale = :ru
  en_eur_currency.update_attributes!(:name => "Евро", :about => "Евро")
end

en_gbp_currency = Currency.find_by_language_id_and_code(english_language.id, "gbp")
if en_gbp_currency.nil?
  I18n.locale = :en
  en_gbp_currency = Currency.create!(:language_id => english_language.id, :name => "GBP", :code => "gbp",
                                      :about => "GBP", :enabled => true)
  I18n.locale = :ru
  en_gbp_currency.update_attributes!(:name => "Английский фунт стерлингов", :about => "Английский фунт стерлингов")
end

ru_rub_currency = Currency.find_by_language_id_and_code(russian_language.id, "rub")
if ru_rub_currency.nil?
  I18n.locale = :en
  ru_rub_currency = Currency.create!(:language_id => russian_language.id, :name => "RUB", :code => "rub",
                                      :about => "RUB", :enabled => true, :prime => true)
  I18n.locale = :ru
  ru_rub_currency.update_attributes!(:name => "Рубль", :about => "Рубль")
end
